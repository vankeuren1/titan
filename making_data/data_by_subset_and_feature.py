import numpy as np
import pandas as pd
from os.path import exists

#----------------------------------------------------
chan=np.load("/home/wadem/omicron_triggers/o3a/chunk01/train_feature_names.npy")
data=np.load("/home/wadem/omicron_triggers/o3a/chunk01/not_whitened_train_features.npy")
df=pd.DataFrame(data,columns=chan)


x=0
y=0
features=np.array(['-snr','-frequency','-deltatime'])
while x<len(chan):
    y=0
    while y<len(features):
        subset=[s[:6] for s in chan]
        loadpath="/home/wayt1/making_data/chunk01/not_whitened_train_features_"+subset[x]+".npy"
        savepath="/home/wayt1/making_data/chunk01/subset_and_feature/not_whitened_train_features_"+subset[x]+features[y]
        print(savepath)
        file_exists = exists(savepath+'.npy')
        print("Path exists:", file_exists)
        if file_exists:
            print("This subset has already been created")
        else:
            print("Loadpath",loadpath)
            data_subsetted=np.load(loadpath)
            print(subset[x])
            yesorno=df.columns.str.startswith(subset[x])
            array=chan[yesorno]
            df2=pd.DataFrame(data_subsetted,columns=array)
            yesorno2=df2.columns.str.endswith(features[y])
            array2=array[yesorno2]
            data2=data_subsetted[:,yesorno2]
            np.save(savepath, data2)
        y+=1
    x+=1
