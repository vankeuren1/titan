
import numpy as np
import pandas as pd
from os.path import exists

#----------------------------------------------------
chan=np.load("/home/wadem/omicron_triggers/o3a/chunk02/train_feature_names.npy")
data=np.load("/home/wadem/omicron_triggers/o3a/chunk02/not_whitened_train_features.npy")
df=pd.DataFrame(data,columns=chan)


x=0
features=np.array(['-snr','-frequency','-deltatime'])
while x<len(features):
    savepath="/home/wayt1/making_data/chunk02/by_feature/not_whitened_train_features"+features[x]
    print(savepath)
    file_exists = exists(savepath+'.npy')
    print("Path exists:", file_exists)
    if file_exists:
        print("This subset has already been created")
    else:
        print("Creating subset")
        yesorno=df.columns.str.endswith(features[x])
        print(features[x])
        array=data[:,yesorno]
        np.save(savepath, array)
    x+=1
