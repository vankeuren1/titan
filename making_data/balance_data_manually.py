import numpy as np

#---------------------------
chunks=['01','02','03']
subsystem=["ASC","CAL","HPI","IMC","ISI","LSC","OMC","PEM","PSL","SQZ","SUS","TCS"]
for i in chunks:
    print("chunk{}".format(i))
    for j in subsystem: 
        print("Subsystem={}".format(j))
        data_CAL=np.load("/home/wayt1/subsystem_data/unseen_test_data/chunk{}/whitened_train_features_H1:CAL.npy".format(i))
        data=np.load("/home/wayt1/subsystem_data/unseen_test_data/chunk{}/whitened_train_features_H1:{}.npy".format(i,j))
        assert len(data_CAL)==len(data)
        labels=np.load("/home/wayt1/data_built/unseen_test_data/labels_chunk{}.npy".format(i))
        labels_cal=np.load("/home/wayt1/subsystem_data/unseen_test_data/chunk{}/labels_chunk{}.npy".format(i,i))

        assert len(labels_cal)==len(labels)

        print("All assertions have been passed")
        sorted_indxs = np.argsort(labels, axis = None)
        data= data[sorted_indxs][::-1]
        labels = labels[sorted_indxs][::-1]
      # First sort the data and organize it so that the glitches come first and cleans come last
        unclean=int(sum(labels))
        clean = int(len(data) - unclean)
        print(f"Length of Data before Balancing:{len(data)}, Glitch:{unclean}, Clean:{clean}")  

        data_glitch=data[:unclean,:]
        label_glitch=labels[:unclean]

        data_clean=data[unclean:,:]
        label_clean=labels[unclean:]

        ratio=int(clean/unclean)
        data_clean = data_clean[::ratio]
        #print(data)
        label_clean = label_clean[::ratio]    #slice as many labels as data

        data=np.concatenate((data_glitch,data_clean),axis=0)
        labels=np.concatenate((label_glitch,label_clean),axis=0)

        #print("Labels after being sliced:",labels)
        # Count number of glitch and clean again
        unclean = int(labels.sum())
        clean = len(data) - unclean
        print(f"Length of Data after Balancing:{len(data)}, Glitch:{unclean}, Clean:{clean}")
        np.save("/home/wayt1/subsystem_data/unseen_test_data/chunk{}/whitened_train_features_H1:{}_balanced.npy".format(i,j),data)
    np.save("/home/wayt1/subsystem_data/unseen_test_data/chunk{}/labels_balanced.npy".format(i),labels)
